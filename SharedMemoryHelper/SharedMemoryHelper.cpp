// SharedMemoryHelper.cpp : Defines the entry point for the console application.
//
#include <WinSock2.h>
#include <Ws2tcpip.h>
//More info about include when using multicast here
//https://support.microsoft.com/en-us/help/257460/info-header-and-library-requirement-when-set-get-socket-options-at-the
#pragma comment(lib,"ws2_32.lib") //Winsock Library
#include <windows.h>
#include <iostream>

// More info:
// https://github.com/gabime/spdlog/issues/878#issuecomment-431451205
#define SPDLOG_LEVEL_NAMES { "[trace   ]", "[debug   ]", "[info    ]", "[warning ]", "[error   ]", "[critical]", "[        ]" }
// Logger
#include <spdlog/spdlog.h>
#include "spdlog/sinks/stdout_color_sinks.h"
#include "spdlog/sinks/rotating_file_sink.h"

#define BUF_SIZE 5500
TCHAR szName[] = TEXT("SharedMemory");


/*****************************/
/*UDP to receive command from CCD*/
int iResultUDP = 0;
u_long iMode = 1; //non-blocking socket mode
WSADATA wsaDataUDP;
sockaddr_in SenderAddrUDP;
int SenderAddrUDPSize = sizeof(SenderAddrUDP);
SOCKET RecvSocketUDP;
sockaddr_in RecvAddrUDP;

unsigned short PortUDP = 8800;
struct primaryData {
	float Airspeed;
	float trueAirspeed;
	float SelectedAirspeed;
	float GroundSpeed;
	float Pitch;
	float Roll;
	float VerticalSpeed;
	float VerticalSpeedBug;
	float backupDisplayAltitude;
	float altitudeAboveGround;
	float SelectedAltitude;
	float Heading;
	float trueHeading;
	float SelectedHeading;
	float Latitude;
	float Longitude;
	float fuelLeftTank;
	float fuelRightTank;
	float fuelCenterTank;
	float N1Left;
	float N1Right;
	float N2Left;
	float N2Right;
	float EGTLeft;
	float EGTRight;
	float OilPressureLeft;
	float OilPressureRight;
	float OilTempLeft;
	float OilTempRight;
	float OilQuantityLeft;
	float OilQuantityRight;
	float FlapsHandle;
	float WindDirection;
	float Windspeed;
	int FlightDirectorStatus;
	float FlightDirectorPitch;
	float FlightDirectorRoll;
	float FuelFlowLeft;
	float FuelFlowRight;
	float GreenArcValue;
	int GreenArcStatus;
	float GreenArcDistance;
	float xtrack;
	float RadioAltimeter;
	int RadioAltimeterStep;
	float VNAV_ERR;
	int RunwayAltitude;
	int waypointAltitude[128];
	int zuluTime[3];
	int flapUpValue;
	int flapUpStatus;
	int flap1Value;
	int flap1Status;
	int flap5Value;
	int flap5Status;
	int flap15Value;
	int flap15Status;
	int flap25Value;
	int flap25Status;
	float maxSpeed;
	float minSpeed;
	int minSpeedStatus;
	int VORLineStatus;
	float VORLineCourse;
	float Course;
	int VOR1_Show;
	char VORName1[24];
	char VORName2[24];
	float VORDistance1;
	float VORDistance2;
	float runwayStart[2];
	float runwayEnd[2];
	float runwaycourse;
	float HdefPath;
	float NAVBearing;
	int DecisionHeight;
	int DecisionHeightStatus;
	int PullUpStatus;
	float Acceleration;
	float ILS_Horizontal_Value;
	float ILS_Vertical_Value;
	int ILS_Show;
	int ILS_Status;
	float VREFSpeed;
	int VREFStatus;
	int onGround;
	char scenario[5];
	float holdHeading1;
	float holdHeading2;
	float holdLegLength;
	float holdRadius;
	int holdStatus;
	float RNP;
	float ANP;
	float V1Speed;
	float V2Speed;
	float VRSpeed;
	float Mach;
	char Fix[5];
	int airspeedIsMach;
	int legs_alt_rest_type[128];
	float TOD;
	float throttleLevel1;
	float throttleLevel2;
	//Route data
	float latitudeArray[128];
	float longitudeArray[128];
	float latitudeArray2[128];
	float longitudeArray2[128];
	char waypointName[24];
	int legNumber;
	int legNumber2;
	float ETA;
	float waypointDistance;
	char legName[512];
	char legName2[512];
	char waypointNameWithType[512]; //List of waypoint that has to be display along with type
	int waypointType[20]; //Type of the waypoint and the route connect to it. 0 for none, 1 for non-active, 2 for active and 3 flyover, 4 for flyover active
	int legModActive; //Whether the route is being modified
	//Auto pilot data
	int autoThrottleStatus;
	int autoPilotSpeedMode;
	int autoPilotHeadingMode;
	int autopilotAltitudeMode;
	int autoPilotHeadingModeArm;
	int autoPilotAltitudeModeArm;
	int CMDAStatus;
	int CMDBStatus;
	//Data that needs to be either pilot or copilot
	//Pilot Side
	float AltitudePilot;
	int mapRangePilot;
	int airportStatusPilot;
	int waypointStatusPilot;
	int vor1StatusPilot;
	int vor2StatusPilot;
	int baroSTDStatusPilot;
	float baroInHgPilot;
	int baroUnitStatusPilot;
	int mapModePilot;
	int navaidStatusPilot;
	int pfdModePilot; //This is for determining whether it's ILS or LNAV/VNAV text
	int vertPathPilot; //This is for determining whether it's ILS or VOR
	char Text_Line1Pilot[10];
	char Text_Line2Pilot[10];
	int BaroBoxPilot;
	//Copilot Side
	float AltitudeCoPilot;
	int mapRangeCoPilot;
	int airportStatusCoPilot;
	int waypointStatusCoPilot;
	int vor1StatusCoPilot;
	int vor2StatusCoPilot;
	int baroSTDStatusCoPilot;
	float baroInHgCoPilot;
	int baroUnitStatusCoPilot;
	int mapModeCoPilot;
	int navaidStatusCoPilot;
	int pfdModeCoPilot;
	int vertPathCoPilot;
	char Text_Line1CoPilot[10];
	char Text_Line2CoPilot[10];
	int BaroBoxCoPilot;
};

primaryData data;
LPCTSTR p_buf;
HANDLE h_map_file;

std::shared_ptr<spdlog::logger> logger;
bool start_shared_memeory()
{
	h_map_file = CreateFileMapping(
		INVALID_HANDLE_VALUE,    // use paging file
		nullptr,                    // default security
		PAGE_READWRITE,          // read/write access
		0,                       // maximum object size (high-order DWORD)
		BUF_SIZE,                // maximum object size (low-order DWORD)
		szName);                 // name of mapping object

	if (h_map_file == nullptr)
	{
		logger->critical("Could not create file mapping object {}.",GetLastError());
		return false;
	}
	else {
		p_buf = static_cast<LPTSTR>(MapViewOfFile(h_map_file, // handle to map object
			FILE_MAP_ALL_ACCESS, // read/write permission
			0,
			0,
			BUF_SIZE));

		if (p_buf == nullptr)
		{
			logger->critical("Could not map view of file {}."), GetLastError();
			CloseHandle(h_map_file);
			return false;
		}
	}
	return true;
}

std::string getLogTimestamp()
{
	std::chrono::system_clock::time_point now = std::chrono::system_clock::now();
	std::chrono::system_clock::duration tp = now.time_since_epoch();

	tp -= std::chrono::duration_cast<std::chrono::seconds>(tp);

	time_t tt = std::chrono::system_clock::to_time_t(now);
	struct tm timeinfo{};
	localtime_s(&timeinfo, &tt);
	char buffer[50];

	sprintf_s(buffer, "%04u-%02u-%02u %02u-%02u-%02u", timeinfo.tm_year + 1900,
		timeinfo.tm_mon + 1, timeinfo.tm_mday, timeinfo.tm_hour, timeinfo.tm_min, timeinfo.tm_sec,
		static_cast<unsigned>(std::chrono::duration_cast<std::chrono::milliseconds>(tp).count()));
	std::string s(buffer);
	return s;
}

bool start_logger()
{
	// Create global logger
	// https://github.com/gabime/spdlog/wiki/2.-Creating-loggers

	std::vector<spdlog::sink_ptr> sinks;

	auto console_sink = std::make_shared<spdlog::sinks::stdout_color_sink_mt>();
	console_sink->set_level(spdlog::level::info);
	console_sink->set_pattern("[%H:%M:%S.%e] %^%l%$ %v");
	sinks.push_back(console_sink);

	std::string logFileName = "Logs\\Helper_Log_";
	logFileName += (getLogTimestamp() + ".txt");

	// Create a rotaion log file with 10MB max size and with maximum of two files
	auto file_sink = std::make_shared<spdlog::sinks::rotating_file_sink_mt>(logFileName, 10 * 1024 * 1024, 2);
	file_sink->set_level(spdlog::level::warn);
	file_sink->set_pattern("[%D %T.%e] %^%l%$ %v ");
	sinks.push_back(file_sink);

	try {
		logger = std::make_shared<spdlog::logger>("MainLogger", begin(sinks), end(sinks));
	}
	catch (const spdlog::spdlog_ex &ex)
	{
		std::cout << "Log init failed: " << ex.what() << std::endl;
		return false;
	}
	// Fllush on warning
	logger->flush_on(spdlog::level::info);
	// Register it so it can be accessed globally
	spdlog::register_logger(logger);

	return true;
}

int main() {


	if (!start_logger() || !start_shared_memeory())
	{
		return 0;
	}
	
	// Initialize Winsock
	iResultUDP = WSAStartup(MAKEWORD(2, 2), &wsaDataUDP);
	if (iResultUDP != NO_ERROR) {
		logger->warn("WSAStartup for UDP failed with error {}", iResultUDP);
	}
	//-----------------------------------------------
	// Create a receiver socket to receive datagrams
	RecvSocketUDP = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (RecvSocketUDP == INVALID_SOCKET) {
		logger->warn("Create UDP socket for listening failed with error {}", WSAGetLastError());
	}

	RecvAddrUDP.sin_family = AF_INET;
	RecvAddrUDP.sin_port = htons(PortUDP);
	RecvAddrUDP.sin_addr.s_addr = htonl(INADDR_ANY);

	// Bind the socket to any address and the specified port.
	iResultUDP = bind(RecvSocketUDP, reinterpret_cast<SOCKADDR *>(&RecvAddrUDP), sizeof(RecvAddrUDP));
	if (iResultUDP != 0) {
		logger->warn("UDP bind failed with error {}", WSAGetLastError());
	}

	//Make the socket non-blocking
	/*iResultUDP = ioctlsocket(RecvSocketUDP, FIONBIO, &iMode);
	if (iResultUDP != NO_ERROR) {
		printf("ioctlsocket failed with error: %ld\n", iResultUDP);
	}*/

		/*
		// use setsockopt() to request that the kernel join a multicast group
		struct ip_mreq mreq;
		//mreq.imr_multiaddr.s_addr = inet_addr("239.0.0.0");
		InetPton(AF_INET, _T("239.0.0.0"), &mreq.imr_multiaddr.s_addr);
		mreq.imr_interface.s_addr = htonl(INADDR_ANY);
		if (setsockopt(RecvSocketUDP, IPPROTO_IP, IP_ADD_MEMBERSHIP, (char*)&mreq, sizeof(mreq)) < 0)
		{
			wprintf(L"Subcribe to multicast failed with error %d\n", WSAGetLastError());
			//return 1;
		}*/

	logger->info("Ready to receive data.");
	//printf("Size: %d\n", sizeof(data));
	while (true) {
		recvfrom(RecvSocketUDP, reinterpret_cast<char*>(&data), sizeof(data), 0, reinterpret_cast<SOCKADDR *>(&SenderAddrUDP), &SenderAddrUDPSize);
		CopyMemory(PVOID(p_buf), &data, sizeof(data));
		//logger->info("Size: {}", sizeof(data));
		//logger->info("Latitude: {}", data.Latitude);
		//logger->info("Longitude: {}", data.Longitude);
		//logger->info("Altitude: {}", data.AltitudePilot);
	}
}